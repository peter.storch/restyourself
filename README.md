# REST Yourself

A REST-Webservice-Developer-Exam. Initially created as a Mystery Geocache [GC2MZPW](https://coord.info/GC2MZPW) at [Geocaching.com](https://www.geocaching.com).

To run the service execute the Main class `RESTYourselfServer`.

The code was written as low-level as possible, as Java Servlets, to learn as much as possible about HTTP and REST. The service was deployed on Google AppEngine and could be accessed via http://restyourself-hrd.appspot.com/exam/geocaching. 

Later on I removed the GWT code and wrapped the simple Servlets with an embedded Jetty server and added some Unittests. The Unittests can now be used to study which requests were expected to solve the puzzle. If you want to try yourself, don't look at it.

## Description
For all who struggle with WS-Deathstar, this is for you: give yourself a REST.

Representational State Transfer (REST) is a style of software architecture for distributed hypermedia systems such as the World Wide Web. The term Representational State Transfer was introduced and defined in 2000 by Roy Fielding in his doctoral dissertation. Fielding is one of the principal authors of the Hypertext Transfer Protocol (HTTP) specification versions 1.0 and 1.1. -- Wikipedia

With this Mystery I wanted to do something differently. I could have made a simple and dry multiple choice test about this topic, but I wanted to do something for real. So I created a real working web application where you can test your REST skills live and get immediate feedback.

### What is needed?

This mystery consists of 10 tasks, which need to be performed against an example geocaching REST API. Each successfully performed task gives you a hint for the final and the next task description.
The 10 Tasks form a short geocaching story:

1. Open the application root ressource
1. Look for Pocketqueries
1. Create your own Pocketquery
1. See the Pocketquery result
1. Open a cache listing
1. Retrieve the logs
1. Refresh the logs
1. Post your own log
1. Correct you log message
1. Cleanup your Pockequery

You need to perform proper HTTP requests, know HTTP methods, status codes and headers. It's also necessary to be familiar with media types like XML and JSON.
The 10 tasks build upon each other. So you need the previous responses to perform the next task.

### Constraints
This API is minimalistic and only designed to work with the exact task you are about to perform. If you are curious, you might try other calls, but don't expect them to work.
It's completely stateless and doesn't remember previous requests. If you make a mistake: just try again.
It talks XML only, so be precise (and simple) with your requests!

### Task 1
Call this URI: http://localhost:8080/exam/geocaching and ask for the xml representation of this ressource.
