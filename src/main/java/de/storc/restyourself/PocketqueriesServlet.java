package de.storc.restyourself;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class PocketqueriesServlet extends AbstractExamServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        assertAcceptXML(req);
        sendXML(resp, "pocketqueries.xml");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        assertContentTypeXML(req);
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            docBuilderFactory.setNamespaceAware(true);
            docBuilderFactory.setValidating(true);
            docBuilderFactory.setAttribute(
                    "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                    "http://www.w3.org/2001/XMLSchema");
            docBuilderFactory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaSource",
                    getClass().getClassLoader().getResourceAsStream("pocketqueries.xsd"));
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            docBuilder.setErrorHandler(new ErrorHandler() {

                @Override
                public void error(SAXParseException arg0) throws SAXException {
                    log.warning(arg0.getMessage());
                    throw arg0;
                }

                @Override
                public void fatalError(SAXParseException arg0) throws SAXException {
                    log.warning(arg0.getMessage());
                    throw arg0;
                }

                @Override
                public void warning(SAXParseException arg0) throws SAXException {
                    log.warning(arg0.getMessage());
                    // ignore
                }
            });
            byte[] body = getBody(req);
            Document doc = docBuilder.parse(new ByteArrayInputStream(body));
            if (!"pocketquery".equals(doc.getDocumentElement().getLocalName())) {
                throw new RuntimeException("Invalid root element!");
            }

            resp.setStatus(HttpServletResponse.SC_CREATED);
            resp.setHeader("Location", "/exam/geocaching/pocketqueries/123456789");
            sendXML(resp, "pocketqueryposted.xml");
        } catch (Exception e) {
            log.severe(e.getMessage());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().append(e.getMessage());
        }
    }

}
