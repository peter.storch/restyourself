package de.storc.restyourself;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

public abstract class AbstractExamServlet extends HttpServlet {

    private static final String APPLICATION_JSON = "application/json";

    private static final String APPLICATION_XML = "application/xml";

    private static final long serialVersionUID = 1L;

    protected final Logger log = Logger.getLogger(getClass().getName());

    protected void assertAcceptXML(HttpServletRequest req) {
        String accept = req.getHeader("accept");
        log.info("Accept: " + accept);
        if (!APPLICATION_XML.equals(accept)) {
            throw new NotAcceptableException(APPLICATION_XML);
        }
    }

    protected void sendXML(HttpServletResponse resp, String file)
            throws IOException {
        resp.addHeader("Content-Type", APPLICATION_XML);
        InputStream is = getClass().getClassLoader().getResourceAsStream(file);
        IOUtils.copy(is, resp.getOutputStream());
    }

    protected void assertContentTypeJSON(HttpServletRequest req) {
        String contentType = extractContentType(req);
        if (!APPLICATION_JSON.equals(contentType)) {
            throw new UnsupportedMediaTypeException();
        }
    }

    protected String extractContentType(HttpServletRequest req) {
        String contentType = req.getContentType();
        log.info("Content-Type: " + contentType);
        if (contentType != null && contentType.contains(";")) {
            contentType = contentType.split(";")[0];
            log.info("MimeType: " + contentType);
        }
        return contentType;
    }

    protected void assertContentTypeXML(HttpServletRequest req) {
        String contentType = extractContentType(req);
        if (!APPLICATION_XML.equals(contentType)) {
            throw new UnsupportedMediaTypeException();
        }
    }

    protected void assertStringValue(JSONObject log, String name, String value) {
        String type = getString(log, name);
        if (!value.equals(type)) {
            throw new RuntimeException("\"" + name + "\" has wrong value");
        }
    }

    protected String getString(JSONObject jsonObj, String name) {
        String value = (String) jsonObj.get(name);
        if (value == null) {
            throw new RuntimeException("\"" + name + "\" is missing");
        }
        return value;
    }

    protected JSONObject getJSONLog(HttpServletRequest req) throws Exception {
        String logJson = new String(getBody(req));
        Object obj = JSONValue.parseWithException(logJson);
        JSONObject log;
        if (obj instanceof JSONObject) {
            log = (JSONObject) obj;
            if (log.containsKey("log")) {
                log = (JSONObject) log.get("log");
            }
        } else {
            throw new RuntimeException("'log' not found");
        }
        return log;
    }

    protected byte[] getBody(HttpServletRequest req) throws IOException {
        byte[] body = IOUtils.toByteArray(req.getInputStream());
        log.info("Body:\n" + new String(body));
        return body;
    }

}
