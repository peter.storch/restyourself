package de.storc.restyourself;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;

public class GeocacheLogsServlet extends AbstractExamServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        assertContentTypeJSON(req);
        try {
            JSONObject log = getJSONLog(req);

            String strDate = getString(log, "date");
            new SimpleDateFormat("yyyy-MM-dd").parse(strDate);

            assertStringValue(log, "type", "found");
            getString(log, "user");
            getString(log, "comment");

            Object trackablesObj = log.get("trackables");
            if (trackablesObj == null) {
                throw new RuntimeException("trackables are missing");
            }

            JSONArray trackables;
            if (trackablesObj instanceof JSONArray) {
                trackables = (JSONArray) trackablesObj;
                if (trackables.isEmpty()) {
                    throw new RuntimeException("trackables are missing");
                }

            } else if (trackablesObj instanceof JSONObject) {
                JSONObject trackablesJsonObj = (JSONObject) trackablesObj;
                Object trackableObj = trackablesJsonObj.get("trackable");
                if (trackableObj instanceof JSONArray) {
                    trackables = (JSONArray) trackableObj;
                } else {
                    throw new RuntimeException("trackable expected to be an Array");
                }
            } else {
                throw new RuntimeException("trackables expected to be an Array");
            }

            for (final Object trackable1 : trackables) {
                final JSONObject trackable = (JSONObject) trackable1;
                assertStringValue(trackable, "id", "yet_another_id");
                assertStringValue(trackable, "name", "Yet Another TB");
                assertStringValue(trackable, "href", "/exam/geocaching/trackables/yet_another_id");
                assertStringValue(trackable, "action", "grabbed");

            }

            resp.setStatus(HttpServletResponse.SC_CREATED);
            resp.setHeader("Location", "/exam/geocaching/geocaches/GC2MZPW/logs/4711");
            sendXML(resp, "logposted.xml");
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().append("Invalid JSON Log Format: ").append(e.toString());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        assertAcceptXML(req);
        if (!"DateDescending".equals(req.getParameter("orderBy"))
                || !"10".equals(req.getParameter("maxLogs"))) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        if ("686897696a7c876b7e".equals(req.getHeader("If-None-Match"))) {
            resp.setHeader("ETag", "123897696a7c876789");
            sendXML(resp, "geocachelogs2.xml");
        } else {
            resp.setHeader("ETag", "686897696a7c876b7e");
            sendXML(resp, "geocachelogs.xml");
        }
    }
}
