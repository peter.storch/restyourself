package de.storc.restyourself;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

/**
 * Jetty Server Wrapper for the RESTYourself WebApplication
 */
public class RESTYourselfServer {

    private final Server server;

    public RESTYourselfServer() {
        server = new Server(8080);
        ServletHandler handler = new ServletHandler();
        server.setHandler(handler);

        handler.addServletWithMapping(ExamServlet.class, "/exam/*");
    }

    public void start() {
        try {
            server.start();
        } catch (Exception e) {
            throw new RuntimeException("Couldn't start server", e);
        }
    }

    public void join() {
        try {
            server.join();
        } catch (InterruptedException e) {
            throw new RuntimeException("Error joining server", e);
        }
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            throw new RuntimeException("Error stopping server", e);
        }
    }

    public static void main(String[] args) throws Exception {
        final RESTYourselfServer restYourselfServer = new RESTYourselfServer();
        restYourselfServer.start();
        restYourselfServer.join();
    }

}
