package de.storc.restyourself;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;

public class GeocacheLogServlet extends AbstractExamServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        assertContentTypeJSON(req);
        try {
            JSONObject log = getJSONLog(req);

            assertMissingForUpdate(log, "date");
            assertMissingForUpdate(log, "type");
            assertMissingForUpdate(log, "user");
            getString(log, "comment");

            Object trackablesObj = log.get("trackables");
            if (trackablesObj != null) {
                if (!(trackablesObj instanceof JSONArray)) {
                    throw new RuntimeException("trackables expected to be an Array");
                }
                JSONArray trackables = (JSONArray) trackablesObj;
                if (!trackables.isEmpty()) {
                    throw new RuntimeException("trackables should be empty for log updates");
                }
            }

            resp.setStatus(HttpServletResponse.SC_CREATED);
            resp.setHeader("Location", "/exam/geocaching/geocaches/GC2MZPW/logs/4711");
            sendXML(resp, "logupdated.xml");
        } catch (final Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().append("Invalid JSON Log Format: ").append(e.toString());
        }

    }

    protected void assertMissingForUpdate(JSONObject jsonObj, String name) {
        if (jsonObj.get(name) != null) {
            throw new RuntimeException("\"" + name + "\" must not be provided for updates");
        }
    }

}
